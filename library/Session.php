<?php

class Session
{
    function __construct()
    {
        // if (session_id() == '')
        // {
        //     session_start();
        // }
    }

    public function setSession($key, $value)
    {
        $_SESSION[$key] = $value;
    }

    public function getSession($key)
    {
        return $_SESSION[$key];
    }

    public function unsetSession($key)
    {
        unset($_SESSION[$key]);
    }

    public function issetSession($key)
    {
        if(isset($_SESSION[$key]))
        {
            return true;
        }else{
            return false;
        }
    }
}
?>