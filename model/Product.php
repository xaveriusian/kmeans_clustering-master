<?php
require_once 'Model.php';

class Product extends Model
{
    public $name = 'Product';
    public $table = 'product';
    public $primaryKey = 'product_id';
    protected $columns = ['name'];
}
?>