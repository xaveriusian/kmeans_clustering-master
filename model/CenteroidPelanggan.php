<?php
require_once 'Model.php';

class CenteroidPelanggan extends Model
{
    public $name = 'Centeroid Pelanggan';
    public $table = 'centeroid_pelanggans';
    public $primaryKey = 'centeroid_pelanggan_id';
    protected $columns = ['cluster', 'recency', 'frequency', 'monetary'];

    public function setCenteroid($centeroid)
    {
        $this->truncate();

        $res = [];
        foreach($centeroid as $i => $row)
        {
            $res[] = $this->create([
                'cluster' => 'C'.($i+1),
                'recency' => $row[0],
                'frequency' => $row[1],
                'monetary' => $row[2],
                ]);
        }
    }

    public function getCenteroid()
    {
        $c1 = $this->select('WHERE cluster="C1" LIMIT 1')[0];
        $c2 = $this->select('WHERE cluster="C2" LIMIT 1')[0];
        $c3 = $this->select('WHERE cluster="C3" LIMIT 1')[0];
        $c4 = $this->select('WHERE cluster="C4" LIMIT 1')[0];

        $res = [
            [
                $c1['recency'],
                $c1['frequency'],
                $c1['monetary'],
            ],[
                $c2['recency'],
                $c2['frequency'],
                $c2['monetary'],
            ],[
                $c3['recency'],
                $c3['frequency'],
                $c3['monetary'],
            ],[
                $c4['recency'],
                $c4['frequency'],
                $c4['monetary'],
            ],
        ];

        return $res;
    }
}
?>