<?php
    // Breadcrumb setup
    $breadcrumb_items = [
        [
            'title' => 'Home',
            'link' => url('/')
        ],
        [
            'title' => 'Data Pelanggan',
            'link' => url('/data_pelanggans')
        ],
        [
            'title' => 'Iterasi K-Means',
            'link' => 'javascript:void(0)'
        ]
    ];

    include_once load_component('breadcrumb');

?>
<br>
<div class="card">
    <div class="card-content">
        <?php
            $model_data = new DataPelanggan();
            $datas = $model_data->selectRFM();

            if(count($datas) > 0)
            {
        ?>
            <!-- Start Data Awal -->
                <div id="man" class="col s12">
                    <div class="card material-table z-depth-2">
                        <div class="table-header">
                            <span class="table-title">Data Awal</span>
                            <div class="actions">
                                <a href="#" class="search-toggle waves-effect btn-flat nopadding"><i class="material-icons">search</i></a>
                            </div>
                        </div>
                        <table class="highlight datatable">
                            <thead>
                                <tr>
                                    <th>Pelanggan</th>
                                    <th>Recency</th>
                                    <th>Frequency</th>
                                    <th>Monetary</th>
                                </tr>
                            </thead>
                            <tbody>
                        <?php
                            foreach ($datas as $row)
                            {
                        ?>
                                <tr>
                                    <td><?=$row['pelanggan']['name']?></td>
                                    <td><?=$row['recency']?></td>
                                    <td><?=$row['frequency']?></td>
                                    <td><?=$row['monetary']?></td>
                                </tr>
                        <?php
                            }
                        ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            <!-- End Data Awal -->

            <!-- Start Iterasi  -->
            <?php
                $centeroidPelanggan = new CenteroidPelanggan();
                $centeroid = $centeroidPelanggan->getCenteroid();
                $last_cluster = [];
                $loop = true;
                $iteration = 1;
                while($loop)
                {
            ?>
                    <div class="card z-depth-2">
                        <div class="card-content">
                        <span class="card-title"><strong>Iterasi ke-<?=$iteration?></strong></span>
                <?php
                    include 'view/table-centeroid.php';
                    $euc = $model_data->getEuclidian($centeroid);
                    include 'view/table-euclidean.php';   
                    $centeroid = $model_data->getNewCenteroid($euc);
                ?>
                        </div>
                    </div>
            <?php
                    $new_cluster = [];
                    foreach ($euc as $row) {
                        $new_cluster[] = $row['cluster'];
                    }
                    if($last_cluster === $new_cluster)
                    {
                        $loop = false;
                    }else{
                        $last_cluster = $new_cluster;
                        $iteration++;
                    }
                }

                $c1 = [];
                $c2 = [];
                $c3 = [];
                $c4 = [];
                foreach ($euc as $row) {
                    if($row['cluster'] === 'C1')
                    {
                        $c1[] = $row['pelanggan'];
                    }
                    if($row['cluster'] === 'C2')
                    {
                        $c2[] = $row['pelanggan'];
                    }
                    if($row['cluster'] === 'C3')
                    {
                        $c3[] = $row['pelanggan'];
                    }
                    if($row['cluster'] === 'C4')
                    {
                        $c4[] = $row['pelanggan'];
                    }
                }
            ?>
            <!-- End Iterasi  -->

            <!-- Start Kesimpulan -->

            <div class="card z-depth-3">
                <div class="card-content">
                    <p>Karena pada Iterasi ke-<?=$iteration?> tidak terjadi perubahan Cluster, maka iterasi dicukupkan sampai iterasi ke-<?=$iteration?>. Dengan keanggotaan Cluster:</p>
                    <ul>
                        <li>Anggota C1 = <?=implode(', ', array_column($c1, 'name'))?></li>
                        <li>Anggota C2 = <?=implode(', ', array_column($c2, 'name'))?></li>
                        <li>Anggota C3 = <?=implode(', ', array_column($c3, 'name'))?></li>
                        <li>Anggota C4 = <?=implode(', ', array_column($c4, 'name'))?></li>
                    </ul>
                </div>
            </div>
            <!-- End Kesimpulan -->

            <!-- Start SSW -->
    <?php
        $ssw = $model_data->getEuclidian($centeroid);
    ?>
            <div class="card z-depth-3">
                <div class="card-content">
                    <span class="card-title"><strong>SSW</strong></span>
                    <div id="man" class="col s12">
                        <div class="card material-table z-depth-2">
                            <div class="table-header">
                                <span class="table-title">Nilai SSW</span>
                                <div class="actions">
                                    <a href="#" class="search-toggle waves-effect btn-flat nopadding"><i class="material-icons">search</i></a>
                                </div>
                            </div>
                            <table class="highlight datatable">
                                <thead>
                                    <tr>
                                        <th>Pelanggan</th>
                                        <th>Jarak</th>
                                        <th>Cluster</th>
                                    </tr>
                                </thead>
                                <tbody>
                            <?php
                                $c1 = [];
                                $c2 = [];
                                $c3 = [];
                                $c4 = [];
                                foreach ($ssw as $row)
                                {
                            ?>
                                    <tr>
                                        <td><?=$row['pelanggan']['name']?></td>
                                        <td><?=$row[$row['cluster']]?></td>
                                        <td><?=$row['cluster']?></td>
                                    </tr>
                            <?php
                                    if($row['cluster'] === 'C1')
                                    {
                                        $c1[] = $row[$row['cluster']];
                                    }
                                    if($row['cluster'] === 'C2')
                                    {
                                        $c2[] = $row[$row['cluster']];
                                    }
                                    if($row['cluster'] === 'C3')
                                    {
                                        $c3[] = $row[$row['cluster']];
                                    }
                                    if($row['cluster'] === 'C4')
                                    {
                                        $c4[] = $row[$row['cluster']];
                                    }
                                }
                            ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <?php
                        $ssw_c1 = array_avg($c1);
                        $ssw_c2 = array_avg($c2);
                        $ssw_c3 = array_avg($c3);
                        $ssw_c4 = array_avg($c4);
                    ?>
                    <div class="card z-depth-3">
                        <div class="card-content">
                            <p>Nilai SSW tiap Cluster:</p>
                            <ul>
                                <li>SSW C1 = <?=$ssw_c1?></li>
                                <li>SSW C2 = <?=$ssw_c2?></li>
                                <li>SSW C3 = <?=$ssw_c3?></li>
                                <li>SSW C4 = <?=$ssw_c4?></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End SSW -->

            <!-- Start SSB -->
            <?php
                $ssb_c1c2 = eucDistance($centeroid[0], $centeroid[1]);
                $ssb_c1c3 = eucDistance($centeroid[0], $centeroid[2]);
                $ssb_c1c4 = eucDistance($centeroid[0], $centeroid[3]);
                $ssb_c2c3 = eucDistance($centeroid[1], $centeroid[2]);
                $ssb_c2c4 = eucDistance($centeroid[1], $centeroid[3]);
                $ssb_c3c4 = eucDistance($centeroid[2], $centeroid[3]);
            ?>
            <div class="card z-depth-3">
                <div class="card-content">
                    <span class="card-title"><strong>SSB</strong></span>
                    <p>Nilai SSB:</p>
                    <ul>
                        <li>SSB C1 - C2 = <?=$ssb_c1c2?></li>
                        <li>SSB C1 - C3 = <?=$ssb_c1c3?></li>
                        <li>SSB C1 - C4 = <?=$ssb_c1c4?></li>
                        <li>SSB C2 - C3 = <?=$ssb_c2c3?></li>
                        <li>SSB C2 - C4 = <?=$ssb_c2c4?></li>
                        <li>SSB C3 - C4 = <?=$ssb_c3c4?></li>
                    </ul>
                </div>
            </div>
            <!-- End SSB -->

            <!-- Start Rasio -->
            <?php
                $rasio_c1c2 = ($ssb_c1c2 > 0) ? ($ssw_c1 + $ssw_c2)/$ssb_c1c2 : 0;
                $rasio_c1c3 = ($ssb_c1c3 > 0) ? ($ssw_c1 + $ssw_c3)/$ssb_c1c3 : 0;
                $rasio_c1c4 = ($ssb_c1c4 > 0) ? ($ssw_c1 + $ssw_c4)/$ssb_c1c4 : 0;
                $rasio_c2c3 = ($ssb_c2c3 > 0) ? ($ssw_c2 + $ssw_c3)/$ssb_c2c3 : 0;
                $rasio_c2c4 = ($ssb_c2c4 > 0) ? ($ssw_c2 + $ssw_c4)/$ssb_c2c4 : 0;
                $rasio_c3c4 = ($ssb_c3c4 > 0) ? ($ssw_c3 + $ssw_c4)/$ssb_c3c4 : 0;
            ?>
            <div class="card z-depth-3">
                <div class="card-content">
                    <span class="card-title"><strong>Rasio</strong></span>
                    <div class="card z-depth-3">
                        <div class="card-content">
                            <span class="card-title"><strong>Rasio C1 - C2</strong></span>
                            <table>
                                <tr>
                                    <td>Rasio C1 - C2</td>
                                    <td>=</td>
                                    <td>(<?=$ssw_c1?> + <?=$ssw_c2?>)/<?=$ssb_c1c2?></td>
                                </tr>
                                <tr>
                                    <td>Rasio C1 - C2</td>
                                    <td>=</td>
                                    <td><?=$rasio_c1c2?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="card z-depth-3">
                        <div class="card-content">
                            <span class="card-title"><strong>Rasio C1 - C3</strong></span>
                            <table>
                                <tr>
                                    <td>Rasio C1 - C3</td>
                                    <td>=</td>
                                    <td>(<?=$ssw_c1?> + <?=$ssw_c3?>)/<?=$ssb_c1c3?></td>
                                </tr>
                                <tr>
                                    <td>Rasio C1 - C3</td>
                                    <td>=</td>
                                    <td><?=$rasio_c1c3?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="card z-depth-3">
                        <div class="card-content">
                            <span class="card-title"><strong>Rasio C1 - C4</strong></span>
                            <table>
                                <tr>
                                    <td>Rasio C1 - C4</td>
                                    <td>=</td>
                                    <td>(<?=$ssw_c1?> + <?=$ssw_c4?>)/<?=$ssb_c1c4?></td>
                                </tr>
                                <tr>
                                    <td>Rasio C1 - C4</td>
                                    <td>=</td>
                                    <td><?=$rasio_c1c4?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="card z-depth-3">
                        <div class="card-content">
                            <span class="card-title"><strong>Rasio C2 - C3</strong></span>
                            <table>
                                <tr>
                                    <td>Rasio C2 - C3</td>
                                    <td>=</td>
                                    <td>(<?=$ssw_c2?> + <?=$ssw_c3?>)/<?=$ssb_c2c3?></td>
                                </tr>
                                <tr>
                                    <td>Rasio C2 - C3</td>
                                    <td>=</td>
                                    <td><?=$rasio_c2c3?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="card z-depth-3">
                        <div class="card-content">
                            <span class="card-title"><strong>Rasio C2 - C4</strong></span>
                            <table>
                                <tr>
                                    <td>Rasio C2 - C4</td>
                                    <td>=</td>
                                    <td>(<?=$ssw_c2?> + <?=$ssw_c4?>)/<?=$ssb_c2c4?></td>
                                </tr>
                                <tr>
                                    <td>Rasio C2 - C4</td>
                                    <td>=</td>
                                    <td><?=$rasio_c2c4?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="card z-depth-3">
                        <div class="card-content">
                            <span class="card-title"><strong>Rasio C3 - C4</strong></span>
                            <table>
                                <tr>
                                    <td>Rasio C3 - C4</td>
                                    <td>=</td>
                                    <td>(<?=$ssw_c3?> + <?=$ssw_c4?>)/<?=$ssb_c3c4?></td>
                                </tr>
                                <tr>
                                    <td>Rasio C3 - C4</td>
                                    <td>=</td>
                                    <td><?=$rasio_c3c4?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="card z-depth-3">
                        <div class="card-content">
                            <p>Nilai Rasio:</p>
                            <ul>
                                <li>Rasio C1 - C2 = <?=$rasio_c1c2?></li>
                                <li>Rasio C1 - C3 = <?=$rasio_c1c3?></li>
                                <li>Rasio C1 - C4 = <?=$rasio_c1c4?></li>
                                <li>Rasio C2 - C3 = <?=$rasio_c2c3?></li>
                                <li>Rasio C2 - C4 = <?=$rasio_c2c4?></li>
                                <li>Rasio C3 - C4 = <?=$rasio_c3c4?></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Rasio -->

            <!-- Start DBI -->
            <div class="card z-depth-3">
                <div class="card-content">
                    <span class="card-title"><strong>DBI</strong></span>
                    <table>
                        <tr>
                            <td>DBI</td>
                            <td>=</td>
                            <td>(1/jumlah_cluster)*(max(rasio))</td>
                        </tr>
                        <tr>
                            <td>DBI</td>
                            <td>=</td>
                            <td>(1/4)*(<?=max([$rasio_c1c2, $rasio_c1c3, $rasio_c1c4, $rasio_c2c3, $rasio_c2c4, $rasio_c3c4])?>)</td>
                        </tr>
                        <tr>
                            <td>DBI</td>
                            <td>=</td>
                            <td><?=(1/3)*max([$rasio_c1c2, $rasio_c1c3, $rasio_c1c4, $rasio_c2c3, $rasio_c2c4, $rasio_c3c4])?></td>
                        </tr>
                    </table>
                </div>
            </div>
            <!-- End DBI -->
        <?php
        }else{
            echo "Access Denied <br><br><a href='#' class='btn-small' onclick='history.back()'>Back</a>";
        }
        ?>
    </div>
</div>