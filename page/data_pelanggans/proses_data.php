<?php
    // Breadcrumb setup
    $breadcrumb_items = [
        [
            'title' => 'Home',
            'link' => url('/')
        ],
        [
            'title' => 'Data Pelanggan',
            'link' => url('/data_pelanggans')
        ],
        [
            'title' => 'Proses Data',
            'link' => 'javascript:void(0)'
        ]
    ];

    include_once load_component('breadcrumb');

?>

<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.3.0/chart.min.js" integrity="sha512-yadYcDSJyQExcKhjKSQOkBKy2BLDoW6WnnGXCAkCoRlpHGpYuVuBqGObf3g/TdB86sSbss1AOP4YlGSb6EKQPg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script> -->
<script src="https://cdn.jsdelivr.net/npm/chart.js@3.3.0/dist/chart.min.js"></script>

<br>
<div class="card">
    <div class="card-content">
        <?php
            $centeroidPelanggan = new CenteroidPelanggan();
            $model_data = new DataPelanggan();
            $datas = $model_data->selectRFM();

            if(count($datas) > 0)
            {
        ?>   
        <!-- Start Data Awal -->
            <div id="man" class="col s12">
                <div class="card material-table z-depth-2">
                    <div class="table-header">
                        <span class="table-title">Data Awal</span>
                        <div class="actions">
                            <a href="#" class="search-toggle waves-effect btn-flat nopadding"><i class="material-icons">search</i></a>
                        </div>
                    </div>
                    <table class="highlight datatable">
                        <thead>
                            <tr>
                                <th>Pelanggan</th>
                                <th>NRecency</th>
                                <th>NFrequency</th>
                                <th>NMonetary</th>
                            </tr>
                        </thead>
                        <tbody>
                    <?php
                        foreach ($datas as $row)
                        {
                    ?>
                            <tr>
                                <td><?=$row['pelanggan']['name']?></td>
                                <td><?=$row['recency']?></td>
                                <td><?=$row['frequency']?></td>
                                <td><?=$row['monetary']?></td>
                            </tr>
                    <?php
                        }
                    ?>
                        </tbody>
                    </table>
                </div>
            </div>
        <!-- End Data Awal -->
            
        <!-- Start Proses Data -->
            <div class="center-align">
                <form action="<?=url('/data_pelanggans')?>" method="post">
                    <input type="hidden" name="model" value="data_pelanggans" id="model"/>
                    <button type="submit" class="btn orange" name="centeroid">PROSES <?=(count($centeroidPelanggan->select()) > 0) ? 'ULANG' : 'DATA'?></button>
                </form>
            </div>
        <!-- End Proses Data -->

            <!-- Start Iterasi  -->
    <?php
            if(count($centeroidPelanggan->select()) > 0)
            {
                $centeroid = $centeroidPelanggan->getCenteroid();
                $last_cluster = [];
                $loop = true;
                $iteration = 1;
                while($loop)
                {
                    $euc = $model_data->getEuclidian($centeroid);
                    $centeroid = $model_data->getNewCenteroid($euc);
                    $new_cluster = [];
                    foreach ($euc as $row) {
                        $new_cluster[] = $row['cluster'];
                    }
                    if($last_cluster === $new_cluster)
                    {
                        $loop = false;
                    }else{
                        $last_cluster = $new_cluster;
                        $iteration++;
                    }
                }

                $c1 = [];
                $c2 = [];
                $c3 = [];
                $c4 = [];
                $c1P = [];
                $c2P = [];
                $c3P = [];
                $c4P = [];
    ?>
                <div id="man" class="col s12">
                    <div class="card material-table z-depth-2">
                        <div class="table-header">
                            <span class="table-title">Hasil Clustering</span>
                            <div class="actions">
                                <a href="#" class="search-toggle waves-effect btn-flat nopadding"><i class="material-icons">search</i></a>
                            </div>
                        </div>
                        <table class="highlight datatable">
                            <thead>
                                <tr>
                                    <th>Pelanggan</th>
                                    <th>Cluster</th>
                                </tr>
                            </thead>
                            <tbody>
                        <?php
                            foreach ($euc as $row)
                            {
                        ?>
                                <tr>
                                    <td><?=$row['pelanggan']['name']?></td>
                                    <td><?=$row['cluster']?></td>
                                </tr>
                        <?php
                                if($row['cluster'] === 'C1')
                                {
                                    $c1[] = $row;
                                    $c1P[] = $row['pelanggan'];
                                }
                                if($row['cluster'] === 'C2')
                                {
                                    $c2[] = $row;
                                    $c2P[] = $row['pelanggan'];
                                }
                                if($row['cluster'] === 'C3')
                                {
                                    $c3[] = $row;
                                    $c3P[] = $row['pelanggan'];
                                }
                                if($row['cluster'] === 'C4')
                                {
                                    $c4[] = $row;
                                    $c4P[] = $row['pelanggan'];
                                }
                            }
                        ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            <!-- End Iterasi  -->

            <!-- Start Kesimpulan -->

            <div class="card z-depth-3">
                <div class="card-content">
                    <span class="card-title"><strong>Kesimpulan:</strong></span>
                    <p>Berdasarkan perhitungan menggunakan K-Means Clustering maka didapatkan hasil bahwa:</p>
                    <ul>
                        <li>Anggota C1 = <?=implode(', ', array_column($c1P, 'name'))?></li>
                        <li>Anggota C2 = <?=implode(', ', array_column($c2P, 'name'))?></li>
                        <li>Anggota C3 = <?=implode(', ', array_column($c3P, 'name'))?></li>
                        <li>Anggota C4 = <?=implode(', ', array_column($c4P, 'name'))?></li>
                    </ul>
                    <br>
                    <hr>
                    <!-- Start Data Grafik -->
                    
                    <?php
                        $dataR = [
                            array_avg(array_column($c1, 'recency')),
                            array_avg(array_column($c2, 'recency')),
                            array_avg(array_column($c3, 'recency')),
                            array_avg(array_column($c4, 'recency')),
                        ];
                        $dataF = [
                            array_avg(array_column($c1, 'frequency')),
                            array_avg(array_column($c2, 'frequency')),
                            array_avg(array_column($c3, 'frequency')),
                            array_avg(array_column($c4, 'frequency')),
                        ];
                        $dataM = [
                            array_avg(array_column($c1, 'monetary')),
                            array_avg(array_column($c2, 'monetary')),
                            array_avg(array_column($c3, 'monetary')),
                            array_avg(array_column($c4, 'monetary')),
                        ];
                    ?>
                    <canvas id="myChart" width="100" height="50"></canvas>
                    <div id="man" class="col s12">
                        <div class="card material-table z-depth-2">
                            <div class="table-header">
                                <span class="table-title">Data Grafik</span>
                            </div>
                            <table class="highlight">
                                <thead>
                                    <tr>
                                        <th>&nbsp;</th>
                                        <th>C1</th>
                                        <th>C2</th>
                                        <th>C3</th>
                                        <th>C4</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th>Rata-rata R</th>
                                        <td><?=array_avg(array_column($c1, 'recency'))?></td>
                                        <td><?=array_avg(array_column($c2, 'recency'))?></td>
                                        <td><?=array_avg(array_column($c3, 'recency'))?></td>
                                        <td><?=array_avg(array_column($c4, 'recency'))?></td>
                                    </tr>
                                    <tr>
                                        <th>Rata-rata F</th>
                                        <td><?=array_avg(array_column($c1, 'frequency'))?></td>
                                        <td><?=array_avg(array_column($c2, 'frequency'))?></td>
                                        <td><?=array_avg(array_column($c3, 'frequency'))?></td>
                                        <td><?=array_avg(array_column($c4, 'frequency'))?></td>
                                    </tr>
                                    <tr>
                                        <th>Rata-rata M</th>
                                        <td><?=array_avg(array_column($c1, 'monetary'))?></td>
                                        <td><?=array_avg(array_column($c2, 'monetary'))?></td>
                                        <td><?=array_avg(array_column($c3, 'monetary'))?></td>
                                        <td><?=array_avg(array_column($c4, 'monetary'))?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <script>
                        var ctx = document.getElementById('myChart');
                        var dataR = <?=json_encode($dataR)?>;

                        var dataF = <?=json_encode($dataF)?>;

                        var dataM = <?=json_encode($dataM)?>;

                        var myChart = new Chart(ctx, {
                            type: 'bar',
                            data: {
                                labels: ['C1', 'C2', 'C3', 'C4'],
                                datasets: [
                                    {
                                        label: 'Rata-rata R',
                                        data: dataR,
                                        borderColor: 'rgb(54, 162, 235)',
                                        backgroundColor: 'rgb(54, 162, 235)',
                                    },
                                    {
                                        label: 'Rata-rata F',
                                        data: dataF,
                                        borderColor: 'rgb(255, 159, 64)',
                                        backgroundColor: 'rgb(255, 159, 64)',
                                    },
                                    {
                                        label: 'Rata-rata M',
                                        data: dataM,
                                        borderColor: 'rgb(201, 203, 207)',
                                        backgroundColor: 'rgb(201, 203, 207)',
                                    }
                                ]
                            },
                            options: {
                                plugins: {
                                    legend: {
                                        position: 'bottom',
                                    },
                                    title: {
                                        display: true,
                                        text: 'Grafik Hasil Clustering'
                                    }
                                }
                            },
                        })
                    </script>
                    <!-- End Data Grafik -->
                </div>
            </div>
            <!-- End Kesimpulan -->
    <?php
            }
        }else{
            if(!count($centeroidPelanggan->select()) > 0 && !count($datas) > 0)
            {
                echo "Access Denied <br><br><a href='#' class='btn-small' onclick='history.back()'>Back</a>";
            }
        }
    ?>
    </div>
</div>