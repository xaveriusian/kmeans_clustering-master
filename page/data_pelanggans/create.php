<?php
    // Breadcrumb setup
    $breadcrumb_items = [
        [
            'title' => 'Home',
            'link' => url('/')
        ],
        [
            'title' => 'Data Pelanggan',
            'link' => url('/data_pelanggans')
        ],
        [
            'title' => 'New Data',
            'link' => 'javascript:void(0)'
        ],
    ];

    include_once load_component('breadcrumb');
?>
<br>
<?php
    include 'view/form.php';
    
?>