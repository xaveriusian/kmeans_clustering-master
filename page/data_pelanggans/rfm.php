<?php
    // Breadcrumb setup
    $breadcrumb_items = [
        [
            'title' => 'Home',
            'link' => url('/')
        ],
        [
            'title' => 'Transformasi RFM',
            'link' => 'javascript:void(0)'
        ]
    ];

    include_once load_component('breadcrumb');

?>
<br>
<div class="card">
    <div class="card-content">
        <!-- Start Data Awal -->
        <?php
            $model_data = new DataPelanggan();
            $datas = $model_data->selectRFM(false);

            if(count($datas) > 0)
            { 
        ?>
                <div id="man" class="col s12">
                    <div class="card material-table z-depth-2">
                        <div class="table-header">
                            <span class="table-title">Transformasi RFM</span>
                            <div class="actions">
                                <a href="#" class="search-toggle waves-effect btn-flat nopadding"><i class="material-icons">search</i></a>
                            </div>
                        </div>
                        <table class="highlight datatable">
                            <thead>
                                <tr>
                                    <th>Pelanggan</th>
                                    <th>Recency</th>
                                    <th>Frequency</th>
                                    <th>Monetary</th>
                                </tr>
                            </thead>
                            <tbody>
                        <?php
                            foreach ($datas as $row)
                            {
                        ?>
                                <tr>
                                    <td><?=$row['pelanggan']['name']?></td>
                                    <td><?=$row['recency']?></td>
                                    <td><?=$row['frequency']?></td>
                                    <td><?=$row['monetary']?></td>
                                </tr>
                        <?php
                            }
                        ?>
                            </tbody>
                        </table>
                    </div>
                </div>
        <?php
            $datas = $model_data->selectRFM();

            if(count($datas) > 0)
            { 
        ?>
                <div id="man" class="col s12">
                    <div class="card material-table z-depth-2">
                        <div class="table-header">
                            <span class="table-title">Normalisasi RFM</span>
                            <div class="actions">
                                <a href="#" class="search-toggle waves-effect btn-flat nopadding"><i class="material-icons">search</i></a>
                            </div>
                        </div>
                        <table class="highlight datatable">
                            <thead>
                                <tr>
                                    <th>Pelanggan</th>
                                    <th>Recency</th>
                                    <th>Frequency</th>
                                    <th>Monetary</th>
                                </tr>
                            </thead>
                            <tbody>
                        <?php
                            foreach ($datas as $row)
                            {
                        ?>
                                <tr>
                                    <td><?=$row['pelanggan']['name']?></td>
                                    <td><?=$row['recency']?></td>
                                    <td><?=$row['frequency']?></td>
                                    <td><?=$row['monetary']?></td>
                                </tr>
                        <?php
                            }
                        ?>
                            </tbody>
                        </table>
                    </div>
                </div>
        <?php
            }
        }else{
            echo "Access Denied <br><br><a href='#' class='btn-small' onclick='history.back()'>Back</a>";
        }
        ?>
    </div>
</div>