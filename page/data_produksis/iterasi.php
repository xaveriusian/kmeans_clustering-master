<?php
    // Breadcrumb setup
    $breadcrumb_items = [
        [
            'title' => 'Home',
            'link' => url('/')
        ],
        [
            'title' => 'Data Produksi',
            'link' => url('/data_produksis')
        ],
        [
            'title' => 'Iterasi K-Means',
            'link' => 'javascript:void(0)'
        ]
    ];

    include_once load_component('breadcrumb');

?>
<br>
<div class="card">
    <div class="card-content">
        <!-- Start Data Awal -->
        <?php
            $model_data = new DataProduksi();
            $datas = $model_data->select();

            if(count($datas) > 0)
            {
                $use_action = false;
                $tableTitle = 'Data Awal';
                include 'view/table.php';
            ?>
            <!-- End Data Awal -->

            <!-- Start Iterasi  -->
            <?php
                $centeroidProduksi = new CenteroidProduksi();
                $centeroid = $centeroidProduksi->getCenteroid();
                $last_cluster = [];
                $loop = true;
                $iteration = 1;
                while($loop)
                {
            ?>
                    <div class="card z-depth-2">
                        <div class="card-content">
                            <span class="card-title"><strong>Iterasi ke-<?=$iteration?></strong></span>
                <?php
                    $table_centeroid_id = 'table-centeroid-'.$iteration;
                    include 'view/table-centeroid.php';
                    $euc = $model_data->getEuclidian($centeroid);
                    $table_euclidean_id = 'table-euclidean-'.$iteration;
                    include 'view/table-euclidean.php';   
                    $centeroid = $model_data->getNewCenteroid($euc);
                ?>
                        </div>
                    </div>
            <?php
                    $new_cluster = [];
                    foreach ($euc as $row) {
                        $new_cluster[] = $row['cluster'];
                    }
                    if($last_cluster === $new_cluster)
                    {
                        $loop = false;
                    }else{
                        $last_cluster = $new_cluster;
                        $iteration++;
                    }
                }

                $c1 = [];
                $c2 = [];
                $c3 = [];
                foreach ($euc as $row) {
                    if($row['cluster'] === 'C1')
                    {
                        $c1[] = $row;
                    }
                    if($row['cluster'] === 'C2')
                    {
                        $c2[] = $row;
                    }
                    if($row['cluster'] === 'C3')
                    {
                        $c3[] = $row;
                    }
                }
            ?>
            <!-- End Iterasi  -->

            <!-- Start Kesimpulan -->

            <div class="card z-depth-3">
                <div class="card-content">
                    <p>Karena pada Iterasi ke-<?=$iteration?> tidak terjadi perubahan Cluster, maka iterasi dicukupkan sampai iterasi ke-<?=$iteration?>. Dengan keanggotaan Cluster:</p>
                    <ul>
                        <li>Anggota C1 = <?=implode(', ', array_column($c1, 'kecamatan'))?></li>
                        <li>Anggota C2 = <?=implode(', ', array_column($c2, 'kecamatan'))?></li>
                        <li>Anggota C3 = <?=implode(', ', array_column($c3, 'kecamatan'))?></li>
                    </ul>
                </div>
            </div>
            <!-- End Kesimpulan -->

            <!-- Start SSW -->
    <?php
        $ssw = $model_data->getEuclidian($centeroid);
    ?>
            <div class="card z-depth-3">
                <div class="card-content">
                    <span class="card-title"><strong>SSW</strong></span>
                    <div id="man" class="col s12">
                        <div class="card material-table z-depth-2">
                            <div class="table-header">
                                <span class="table-title">Nilai SSW</span>
                                <div class="actions">
                                    <a href="#" class="search-toggle waves-effect btn-flat nopadding"><i class="material-icons">search</i></a>
                                </div>
                            </div>
                            <table class="highlight datatable">
                                <thead>
                                    <tr>
                                        <th>Kecamatan</th>
                                        <th>Jarak</th>
                                        <th>Cluster</th>
                                    </tr>
                                </thead>
                                <tbody>
                            <?php
                                $c1 = [];
                                $c2 = [];
                                $c3 = [];
                                foreach ($ssw as $row)
                                {
                            ?>
                                    <tr>
                                        <td><?=$row['kecamatan']?></td>
                                        <td><?=$row[$row['cluster']]?></td>
                                        <td><?=$row['cluster']?></td>
                                    </tr>
                            <?php
                                    if($row['cluster'] === 'C1')
                                    {
                                        $c1[] = $row[$row['cluster']];
                                    }
                                    if($row['cluster'] === 'C2')
                                    {
                                        $c2[] = $row[$row['cluster']];
                                    }
                                    if($row['cluster'] === 'C3')
                                    {
                                        $c3[] = $row[$row['cluster']];
                                    }
                                }
                            ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <?php
                        $ssw_c1 = array_avg($c1);
                        $ssw_c2 = array_avg($c2);
                        $ssw_c3 = array_avg($c3);
                    ?>
                    <div class="card z-depth-3">
                        <div class="card-content">
                            <p>Nilai SSW tiap Cluster:</p>
                            <ul>
                                <li>SSW C1 = <?=$ssw_c1?></li>
                                <li>SSW C2 = <?=$ssw_c2?></li>
                                <li>SSW C3 = <?=$ssw_c3?></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End SSW -->

            <!-- Start SSB -->
            <?php
                $ssb_c1c2 = eucDistance($centeroid[0], $centeroid[1]);
                $ssb_c1c3 = eucDistance($centeroid[0], $centeroid[2]);
                $ssb_c2c3 = eucDistance($centeroid[1], $centeroid[2]);
            ?>
            <div class="card z-depth-3">
                <div class="card-content">
                    <span class="card-title"><strong>SSB</strong></span>
                    <p>Nilai SSB:</p>
                    <ul>
                        <li>SSB C1 - C2 = <?=$ssb_c1c2?></li>
                        <li>SSB C1 - C3 = <?=$ssb_c1c3?></li>
                        <li>SSB C2 - C3 = <?=$ssb_c2c3?></li>
                    </ul>
                </div>
            </div>
            <!-- End SSB -->

            <!-- Start Rasio -->
            <?php
                $rasio_c1c2 = ($ssb_c1c2 > 0) ? ($ssw_c1 + $ssw_c2)/$ssb_c1c2 : 0;
                $rasio_c1c3 = ($ssb_c1c3 > 0) ? ($ssw_c1 + $ssw_c3)/$ssb_c1c3 : 0;
                $rasio_c2c3 = ($ssb_c2c3 > 0) ? ($ssw_c2 + $ssw_c3)/$ssb_c2c3 : 0;
            ?>
            <div class="card z-depth-3">
                <div class="card-content">
                    <span class="card-title"><strong>Rasio</strong></span>
                    <div class="card z-depth-3">
                        <div class="card-content">
                            <span class="card-title"><strong>Rasio C1 - C2</strong></span>
                            <table>
                                <tr>
                                    <td>Rasio C1 - C2</td>
                                    <td>=</td>
                                    <td>(<?=$ssw_c1?> + <?=$ssw_c2?>)/<?=$ssb_c1c2?></td>
                                </tr>
                                <tr>
                                    <td>Rasio C1 - C2</td>
                                    <td>=</td>
                                    <td><?=$rasio_c1c2?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="card z-depth-3">
                        <div class="card-content">
                            <span class="card-title"><strong>Rasio C1 - C3</strong></span>
                            <table>
                                <tr>
                                    <td>Rasio C1 - C3</td>
                                    <td>=</td>
                                    <td>(<?=$ssw_c1?> + <?=$ssw_c3?>)/<?=$ssb_c1c3?></td>
                                </tr>
                                <tr>
                                    <td>Rasio C1 - C3</td>
                                    <td>=</td>
                                    <td><?=$rasio_c1c3?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="card z-depth-3">
                        <div class="card-content">
                            <span class="card-title"><strong>Rasio C2 - C3</strong></span>
                            <table>
                                <tr>
                                    <td>Rasio C2 - C3</td>
                                    <td>=</td>
                                    <td>(<?=$ssw_c2?> + <?=$ssw_c3?>)/<?=$ssb_c2c3?></td>
                                </tr>
                                <tr>
                                    <td>Rasio C2 - C3</td>
                                    <td>=</td>
                                    <td><?=$rasio_c2c3?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="card z-depth-3">
                        <div class="card-content">
                            <p>Nilai Rasio:</p>
                            <ul>
                                <li>Rasio C1 - C2 = <?=$rasio_c1c2?></li>
                                <li>Rasio C1 - C3 = <?=$rasio_c1c3?></li>
                                <li>Rasio C2 - C3 = <?=$rasio_c2c3?></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Rasio -->

            <!-- Start DBI -->
            <div class="card z-depth-3">
                <div class="card-content">
                    <span class="card-title"><strong>DBI</strong></span>
                    <table>
                        <tr>
                            <td>DBI</td>
                            <td>=</td>
                            <td>(1/jumlah_cluster)*(max(rasio))</td>
                        </tr>
                        <tr>
                            <td>DBI</td>
                            <td>=</td>
                            <td>(1/3)*(<?=max([$rasio_c1c2, $rasio_c1c3, $rasio_c2c3])?>)</td>
                        </tr>
                        <tr>
                            <td>DBI</td>
                            <td>=</td>
                            <td><?=(1/3)*max([$rasio_c1c2, $rasio_c1c3, $rasio_c2c3])?></td>
                        </tr>
                    </table>
                </div>
            </div>
            <!-- End DBI -->
        <?php
        }else{
            echo "Access Denied <br><br><a href='#' class='btn-small' onclick='history.back()'>Back</a>";
        }
        ?>
    </div>
</div>