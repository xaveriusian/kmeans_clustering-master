<form class="card z-depth-3" action="<?=(($data ?? '') != '') ? url('/data_produksis/update', $data['data_produksi_id']) : url('/data_produksis/store')?>" method="post">
    <input type="hidden" name="model" value="data_produksis" id="model"/>
    <div class="card-content row">
        <div class="col s12 m8 offset-m2">
            <div class="input-field" style="display:none;">
                <input name="data_produksi_id" id="data_produksi_id" value="<?= $data['data_produksi_id'] ?? ''?>" type="text" class="validate" readonly>
                <label for="data_produksi_id">Data ID</label>
            </div>
            <div class="input-field">
                <input name="kecamatan" id="kecamatan" value="<?= $data['kecamatan'] ?? ''?>" type="text" class="validate" required autofocus>
                <label for="kecamatan">Kecamatan</label>
            </div>
            <?php
              foreach($sayur as $k => $n)
              {
            ?>
              <div class="input-field">
                  <input name="<?=$k?>" id="<?=$k?>" value="<?= $data[$k] ?? '0'?>" type="text" class="validate floatTextBox">
                  <label for="<?=$k?>"><?=$n?></label>
              </div>
            <?php
              }
            ?>
        </div>
    </div>
    <div class="card-action row">
        <div class="col s12 m8 offset-2">
            <button id="button-submit" type="submit" name="<?=(($data ?? '') != '') ? 'update' : 'store'?>" class="btn green"><?=(($data ?? '') != '') ? 'Update' : 'Save'?></button>
        </div>
    </div>
</form>

<script>
// Restricts input for the set of matched elements to the given inputFilter function.
(function($) {
  $.fn.inputFilter = function(inputFilter) {
    return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
      if (inputFilter(this.value)) {
        this.oldValue = this.value;
        this.oldSelectionStart = this.selectionStart;
        this.oldSelectionEnd = this.selectionEnd;
      } else if (this.hasOwnProperty("oldValue")) {
        this.value = this.oldValue;
        this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
      } else {
        this.value = "";
      }
    });
  };
}(jQuery));

$(".floatTextBox").inputFilter(function(value) {
  return /^-?\d*[.]?\d*$/.test(value); });
</script>