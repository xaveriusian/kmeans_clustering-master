<?php
    // Role Permission 
    if($auth->role != 'admin')
    {
        echo "<script>window.location.replace('".url('/')."')</script>";
        exit;
    }

    $model_user = new User();
    $users = $model_user->select();

    // Breadcrumb setup
    $breadcrumb_items = [
        [
            'title' => 'Home',
            'link' => url('/')
        ],
        [
            'title' => 'Users',
            'link' => 'javascript:void(0)'
        ]
    ];

    include_once load_component('breadcrumb');

    // Floating Button Setup
    $button_items = [
        [
            'name' => 'Export Data',
            'icon' => 'file_download',
            'class' => 'purple',
            'link' => url('/users/export')
        ],
        [
            'name' => 'Import Data',
            'icon' => 'insert_drive_file',
            'class' => 'blue modal-trigger',
            'link' => '#modal-users'
        ],
        [
            'name' => 'Input Manual',
            'icon' => 'add',
            'class' => 'green',
            'link' => url('/users/create')
        ]
    ];
    include_once load_component('floating-button');
?>
<br>
<?php
    include 'view/table.php';

    $modal = [
        'id' => 'modal-users',
        'model' => 'users',
        'text' => 'Excel Template : <a href="'.public_url('template/users-template.xlsx').'" download/>users-template.xlsx</a>',
        'action' => url('/users/import'),
    ];

    include_once load_component('modal-import');
?>    