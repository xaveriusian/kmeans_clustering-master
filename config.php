<?php
/**
 * Public Variable
 */
$protocol   = 'http'; # 'http' / 'https'
$url        = 'localhost/kmeans_clustering-master'; # example : 'localhost', '127.0.0.1', 'example.com'
$app_name   = 'kmeans_clustering-master';
$root_path  = '/Applications/XAMPP/htdocs/';

/**
 * Database Configuration
 */
$db_host    = 'localhost';
$db_user    = 'root';
$db_pass    = '';
$db_name    = 'kmeans_clustering';
?>