<?php
require_once 'init.php';

header('location: '.url('/'));

$command = "mysql ";
if (trim($db_user) != '')
{
    $command = $command." -u {$db_user}";
}
if (trim($db_pass) != '')
{
    $command = $command." -p {$db_pass}";
}
if (trim($db_host) != '')
{
    $command = $command." -h {$db_host}";
}

$command = $command." < ";

$output = shell_exec($command . ROOT.'/database/kmeans_clustering.sql');

?>