<?php
if(isset($_GET['export']))
{
    $request = getRequest();
    
    $model = new DataProduksi();
    $export = $model->export();
    
    if($export)
    {
        $session->setSession('success', 'Success export Data Produksi!');
    }else{
        $session->setSession('error', 'Failed export Data Produksi!');
    }
}

if(isset($_POST['import']))
{
    $request = getRequest();
    
    $file = $_FILES['file_import'];

    $upload = fileUpload($file);

    if(!$upload)
    {
        $session->setSession('error', 'Failed import Data Produksi!');
        echo "<script>window.location.replace('".url('/data_produksis')."')</script>";
        exit;
    }

    $model = new DataProduksi();
    $import = $model->import($upload['dirname'].'/'.$upload['basename'], $upload['extension']);
    
    if(!empty($import))
    {
        $session->setSession('success', 'Success import Data Produksi!');
    }
}

if(isset($_POST['centeroid']))
{
    $model_data = new DataProduksi();
    $centeroid = $model_data->getFirstCenteroid();

    if(!empty($centeroid))
    {
        $session->setSession('success', 'Success Proses Data Produksi!');
        echo "<script>window.location.replace('".url('/data_produksis/proses_data')."')</script>";
        exit;
    }
}

if(isset($_POST['store']))
{
    $request = getRequest();
    
    $model = new DataProduksi();
    // Get Average
    $sayur = [
        $request['bawang_daun'],
        $request['bawang_merah'],
        $request['bayam'],
        $request['kacang_panjang'],
        $request['kembang_kol'],
        $request['labu_siam'],
        $request['sawi'],
        $request['cabai_besar'],
        $request['cabai_rawit'],
        $request['ketimun'],
        $request['tomat'],
        $request['terung'],
        $request['kangkung'],
    ];
    $request[htmlspecialchars('average')] = htmlspecialchars(array_avg($sayur));
    $model = $model->create($request);

    if(!empty($model))
    {
        $session->setSession('success', 'Success create New Data Produksi!');
    }
}

if(isset($_POST['destroy']))
{
    $request = getRequest();
    if(!isset($_POST['data_produksi_id'])){
        $session->setSession('warning', 'Data Produksi ID not identified!');
    }else{
        $model = new DataProduksi();
        if($model->delete($_POST['data_produksi_id']))
        {
            $session->setSession('success', 'Success delete Data Produksi!');
        }else{
            $session->setSession('warning', 'Failed delete Data Produksi!');
        }
    }
}

if(isset($_POST['update']))
{
    $request = getRequest();
    if(!isset($_POST['data_produksi_id'])){
        $session->setSession('warning', 'Data Produksi ID tidak teridentifikasi!');
    }else{
        $model = new DataProduksi();
        // Get Average
        $sayur = [
            $request['bawang_daun'],
            $request['bawang_merah'],
            $request['bayam'],
            $request['kacang_panjang'],
            $request['kembang_kol'],
            $request['labu_siam'],
            $request['sawi'],
            $request['cabai_besar'],
            $request['cabai_rawit'],
            $request['ketimun'],
            $request['tomat'],
            $request['terung'],
            $request['kangkung'],
        ];
        $request[htmlspecialchars('average')] = htmlspecialchars(array_avg($sayur));
        $model = $model->update($_POST['data_produksi_id'], $request);

        if(!empty($model))
        {
            $session->setSession('success', 'Success edit Data Produksi!');
        }else{
            $session->setSession('warning', 'Failed edit Data Produksi!');
        }
    }
}

echo "<script>window.location.replace('".url('/data_produksis')."')</script>";
exit;

?>